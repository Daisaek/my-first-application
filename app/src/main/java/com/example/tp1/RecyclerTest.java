package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class RecyclerTest extends AppCompatActivity {

    private static String [] listAni = new String[7];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        listAni = AnimalList.getNameArray();

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerAni);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            //Create a row and set it in row_test
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row_test, parent,false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            //link the item and the view on a row
            String img = AnimalList.getAnimal(listAni[position]).getImgFile();
            holder.bindModel(getResources().getIdentifier(img, "drawable", getPackageName()), listAni[position]);

        }

        @Override
        public int getItemCount() {

            return(listAni.length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView label = null;
        ImageView icon = null;

        RowHolder(View row) {
            //Build the view of each row for each animal in the recyclerView
            super(row);
            label = (TextView)row.findViewById(R.id.label);
            icon = (ImageView)row.findViewById(R.id.icon);
            row.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            //Display information on the animal when you click
            final String item = label.getText().toString();
            Intent intent = new Intent(RecyclerTest.this, DisplayAnimal.class);
            intent.putExtra("name", item);
            startActivity(intent);
        }

        void bindModel(int imgAni, String item) {
            //set the name and the image of the item
            label.setText(item);
            icon.setImageResource(imgAni);
        }
    }
}
