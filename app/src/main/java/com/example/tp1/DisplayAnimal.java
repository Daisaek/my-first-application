package com.example.tp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DisplayAnimal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        //get the name given by the intent
        Intent intent = getIntent();
        String nameA = intent.getStringExtra("name");

        final Animal ani = AnimalList.getAnimal(nameA);
        //get information of the animal
        String yearsMax = ani.getStrHightestLifespan();
        String gestation = ani.getStrGestationPeriod();
        String birthWeight = ani.getStrBirthWeight();
        String adultWeight = ani.getStrAdultWeight();
        String conservStatus = ani.getConservationStatus();
        String img = ani.getImgFile();

        //Set a view for each information and for the image
        TextView title = findViewById(R.id.name);
        title.setText(nameA);

        TextView vie = findViewById(R.id.vie);
        vie.setText(yearsMax);

        TextView gesta = findViewById(R.id.gesta);
        gesta.setText(gestation);

        TextView birthW = findViewById(R.id.pnaissance);
        birthW.setText(birthWeight);

        TextView adultW = findViewById(R.id.padulte);
        adultW.setText(adultWeight);

        final TextView status = findViewById(R.id.statut);
        status.setText(conservStatus);

        ImageView avatar = findViewById(R.id.avatar);
        avatar.setImageResource(getResources().getIdentifier(img, "drawable", getPackageName()));

        //when you click on the button, you will save the new status of th animal
        Button button = (Button)findViewById(R.id.save);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ani.setConservationStatus(status.getText().toString());
                finish();
            }
        });
    }
}
