package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView LIST = findViewById(R.id.listAnimal);
        String [] listName = new String[7];
        listName = AnimalList.getNameArray();
        final ArrayAdapter<String> ADAPTER = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listName);
        LIST.setAdapter(ADAPTER);

        LIST.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final String nameAnimal = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, DisplayAnimal.class);
                intent.putExtra("name", nameAnimal);
                startActivity(intent);
            }
        });
    }
}